package com.example.json;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView output = (TextView) findViewById(R.id.output);
        final Button btnparsejson = (Button) findViewById(R.id.btnparsejson);
/************ Static JSON data ***********/
        final String strJson = "{ \"Employee\":[{\"name\":\"John\"," +
                "\"id\":\"01\"," +
                "\"salary\":\"30000\"}," +
                "{\"name\":\"Smith\"," +
                "\"id\":\"02\"," +
                "\"salary\":\"50000\"}] }";
        String dataToBeParsed = "Click on button to parse JSON.\n\n JSON DATA : \n\n" +
                strJson;
        output.setText(dataToBeParsed);
        btnparsejson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String OutputData = "";
                JSONObject jsonResponse;
                try {
                    jsonResponse = new JSONObject(strJson);
                    JSONArray jsonMainNode = jsonResponse.optJSONArray("Employee");
                    int lengthJsonArr = jsonMainNode.length();
                    for (int i = 0; i<lengthJsonArr; i++) {
/****** Get Object for each JSON node.***********/
                        JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);

                        String id = jsonChildNode.optString("id").toString();
                        String name = jsonChildNode.optString("name").toString();
                        float salary = Float.parseFloat(jsonChildNode.optString("salary").toString());
                        OutputData += "Node " + i + " :\n\n " + id + " | "
                                + name + " | "
                                + salary + " \n\n ";
                        Log.i("JSON", name);
/************ Show Output on screen/activity **********/
                        output.setText(OutputData);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}